#%% - Load Classifier ----------------------------------------------------------

from sklearn import svm, naive_bayes
import pickle
import os

#Load classifier models
model_path = os.getcwd() + "/models/"
clf_SVM = pickle.load(open(model_path + "SVM.pickle", "rb"))
clf_NB = pickle.load(open(model_path + "NB.pickle", "rb"))


#%% - Prediction Function ------------------------------------------------------

import numpy as np
import librosa
import pandas as pd

noise, sr_n = librosa.load("noise.wav", sr = None, mono = True)

def envelope(y,rate,threshold):
    mask = []
    y = pd.Series(y).apply(np.abs)
    y_mean = y.rolling(window=int(rate/10),min_periods=1,center = True).mean()
    for mean in y_mean :
        if mean > threshold:
            mask.append(True)
        else:
            mask.append(False)
    return mask

AMPLIFICATION = 10

def Prediction(RECORD2):
    X, sr_X = librosa.load(RECORD2, sr = None, mono = True)
    X = AMPLIFICATION*X
    noise = AMPLIFICATION*noise
    X = nr.reduce_noise(audio_clip = X,
                        noise_clip = noise,
                        verbose = False)
    mask = envelope(X,sr_X,threshold = 0.0005)
    X = X[mask]
    to_append = []
    #FEATURE EXTRACTION
    spec_cent = librosa.feature.spectral_centroid(y=X, sr=sr_X)
    chroma = librosa.feature.chroma_stft(y=X, sr=sr_X)
    #VERSAO 0.4.2 librosa.feature.rmse
    rmse = librosa.feature.rms(y=X)
    mfcc = librosa.feature.mfcc(y = X, sr = sr_X, n_mfcc = 13)
    to_append += {np.mean(spec_cent)}
    to_append += {np.mean(chroma)}
    to_append += {np.mean(rmse)}
    for n in mfcc: to_append += {np.mean(n)}
    result = clf_SVM.predict(np.reshape(to_append, (1, 16)))
    return result

def Amplification(RECORD, RECORD2):
    X, sr_X = librosa.load(RECORD, sr = None, mono = True)
    X_amp = AMPLIFICATION*X
    librosa.output.write_wav(RECORD2, X_amp, sr_X)

#%% - Realtime Prediction ------------------------------------------------------

import pyaudio
import wave

FORMAT = pyaudio.paInt16        #sampling size and format
CHANNELS = 1                    #mono
RATE = 44100                    #samples per second (sampling rate)
CHUNK = 1024*2                #bytes of data in a buffer; instant storage

RECORD = "recorded1.wav"
RECORD2 = "recorded2.wav"
RECORD_TIME = 3

#pyaudio class instance
p = pyaudio.PyAudio()
#stream object to get data from mic
stream = p.open(format = FORMAT, channels = CHANNELS, rate = RATE,
                frames_per_buffer = CHUNK, input = True)

# Open the connection and start streaming the data
stream.start_stream()
print("* recording\n")
print("\n+---------------------------------+")
print("| Press Ctrl+C to Break Recording |")
print("+---------------------------------+\n")


for j in range(1, 20):
    try:
        print(j)
        frames = []
        for i in range(0, int(RATE / CHUNK * RECORD_TIME)):   #active for +-3 seconds
            mic = stream.read(CHUNK, exception_on_overflow = False)
            frames.append(mic)

        wf = wave.open(RECORD, 'wb')
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(p.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(b''.join(frames))
        wf.close()

        AAA = Amplification(RECORD, RECORD2)
        prediction = Prediction(RECORD2)
        print(prediction)

    except KeyboardInterrupt:
        break
    except:
        pass

#Close stream
stream.stop_stream()
stream.close()
p.terminate()
