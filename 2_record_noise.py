#%% - Record WAV file of mic signal for x seconds ------------------------------

import pyaudio
import numpy as np
import wave
import os

FORMAT = pyaudio.paInt16        #sampling size and format
CHANNELS = 1                    #mono
RATE = 44100                    #samples per second (sampling rate)
CHUNK = 1024*2                #bytes of data in a buffer; instant storage
RECORD_SECONDS = 6              #number of seconds active

LOCATION = os.getcwd() + "/noise.wav"

#pyaudio class instance
p = pyaudio.PyAudio()
#stream object to get data from mic
stream = p.open(format = FORMAT, channels = CHANNELS, rate = RATE,
                frames_per_buffer = CHUNK, input = True, output = True)

# Open the connection and start streaming the data
stream.start_stream()
print("* recording\n")
print("\n+---------------------------------+")
print("| Press Ctrl+C to Break Recording |")
print("+---------------------------------+\n")

#Vector to save the data in recorded audio
frames = []

for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):   #active for +-5 seconds
    try:
        data = stream.read(CHUNK, exception_on_overflow = False)
        frames.append(data)
    except KeyboardInterrupt:
        break
    except:
        pass

print("\n* done recording")

# Close up shop (currently not used because KeyboardInterrupt
# is the only way to close)
stream.stop_stream()
stream.close()
p.terminate()

wf = wave.open(LOCATION, 'wb')
wf.setnchannels(CHANNELS)
wf.setsampwidth(p.get_sample_size(FORMAT))
wf.setframerate(RATE)
wf.writeframes(b''.join(frames))
wf.close()
