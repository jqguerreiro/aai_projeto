1 - Correr o final_record_dataset, gera x ficheiros WAV. Agrupar os ficheiros pelas pastas:
snaps_noisy; claps_noisy; whistles_noisy.
2 - Correr o final_record_noise, gera um ficheiro de ruido WAV.
3 - Correr o final_dataset_preprocess, gera os novos ficheiros de audio WAV, sem
ruído na pasta datasets.
4 - Correr o final_feature_extraction, gera o ficheiro "data.csv" com os dados
dos ficheiros AUDIO.
