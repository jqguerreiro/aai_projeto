#%% - Create Classifier --------------------------------------------------------

import pandas as pd
import numpy as np
from sklearn import svm, naive_bayes
import pickle
import os

CSV_FILE = os.getcwd() + "/data.csv"

#Read dataset and drop unaccessary column
data = pd.read_csv(CSV_FILE)
data = data.drop(["filename"], axis = 1)

#Create the data array (X) and target array (Y)
train = np.array(data.iloc[:, :-1], dtype = float)
target = np.array(data.iloc[:,-1])

#Create the SVM and NaiveBayes classifiers
clf_SVM = svm.SVC()
clf_NB = naive_bayes.GaussianNB()

#Train the classifiers
clf_SVM.fit(train, target)
clf_NB.fit(train, target)

#Save the classifier models
model_path = os.getcwd() + "/models/"
pickle.dump(clf_SVM, open(model_path + "SVM.pickle", "wb"))
pickle.dump(clf_NB, open(model_path + "NB.pickle", "wb"))
