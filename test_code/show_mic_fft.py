#%% - SHOW THE AUDIO WAVE AND FFT for x seconds --------------------------------

#%% - Imports ------------------------------------------------------------------

import pyaudio
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft


#%% - Wave Signal and FFT plot for 5 seconds -----------------------------------

FORMAT = pyaudio.paInt16        #sampling size and format
CHANNELS = 1                    #mono
RATE = 44100                    #samples per second (sampling rate)
CHUNK = 1024                    #bytes of data in a buffer; instant storage
RECORD_SECONDS = 5              #number of seconds active

#pyaudio class instance
p = pyaudio.PyAudio()
#stream object to get data from mic
stream = p.open(format = FORMAT, channels = CHANNELS, rate = RATE,
                frames_per_buffer = CHUNK, input = True, output = True)

#create the plot instance with figure and axis
fig, (ax, ax_fft) = plt.subplots(2)

#variables for plotting
x = np.arange(10000)
y = np.random.randn(10000)
# x_fft = np.linspace(0, RATE, CHUNK)

#create the line object and ax ax_fft plot settings
line, = ax.plot(x, y)
line_fft, = ax_fft.semilogx(x, y)
ax.set_xlim(0, CHUNK)
ax.set_ylim(-2**15, 2**15)
ax.set_xlabel("samples")
ax.set_ylabel("volume")
ax.set_title("Raw Audio Data")
ax_fft.set_xlim(20, RATE/2)

ax_fft.set_title("FFT Spectra")
ax_fft.set_xlabel("frequency(Hz)")
ax_fft.set_ylabel("magnitude")

# Show the plot, but without blocking updates
plt.pause(0.01)
plt.tight_layout()

# Open the connection and start streaming the data
stream.start_stream()
print("\n+---------------------------------+")
print("| Press Ctrl+C to Break Recording |")
print("+---------------------------------+\n")


for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):   #active for +-5 seconds
    try:
        data = stream.read(CHUNK, exception_on_overflow = False)
        data_int = np.frombuffer(data, dtype = np.int16)    #dtype = "h" better?

        line.set_xdata(np.arange(len(data_int)))
        line.set_ydata(data_int)

        # Fast Fourier Transform, 10*log10(abs) is to scale it to dB
        ydata_fft = 10.*np.log10(abs(np.fft.rfft(data_int)))
        line_fft.set_xdata(np.arange(len(ydata_fft)))
        line_fft.set_ydata(ydata_fft)

        plt.pause(0.01)
    except KeyboardInterrupt:
        break
    except:
        pass

# Close up shop (currently not used because KeyboardInterrupt
# is the only way to close)
stream.stop_stream()
stream.close()

p.terminate()
