#%% - SOUND IN TERMINAL ##### --------------------------------------------------

#%% - Imports ------------------------------------------------------------------

import pyaudio
import numpy as np
import matplotlib.pyplot as plt


#%% - Returns sound in terminal forever # --------------------------------------

FORMAT = pyaudio.paInt16        #sampling size and format
CHANNELS = 1                    #mono
RATE = 44100                    #samples per second (sampling rate)
CHUNK = 1024                    #bytes of data in a buffer; instant storage


#pyaudio class instance
p = pyaudio.PyAudio()
#stream object to get data from mic
stream = p.open(format = FORMAT, channels = CHANNELS, rate = RATE,
                frames_per_buffer = CHUNK, input = True, output = True)

while True:
    data = stream.read(CHUNK)
    data = np.frombuffer(data, dtype = np.int16)  #convert bytes output from mic
    peak = np.average(np.abs(data))*5             #dtype = "h" better?
    bars = "#"*int(50*peak/2**16)
    print("%05d %s" % (peak,bars))

stream.stop_stream()
stream.close()
p.terminate()
