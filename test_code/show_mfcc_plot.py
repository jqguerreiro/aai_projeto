# sound classification

import pandas as pd
import os
import librosa
import librosa.display
import matplotlib.pyplot as plt

filename = "clap4.wav"
sig, sr = librosa.load(filename, sr = None, mono = True)
mfcc = librosa.feature.mfcc(y = sig, sr = sr)
mfcc_log = librosa.feature.mfcc(S = librosa.power_to_db(mfcc))


plt.figure(1)
librosa.display.specshow(mfcc, x_axis = "time")
plt.colorbar()
plt.title("MFCC")
plt.tight_layout()
plt.show()

plt.figure(2)
librosa.display.specshow(mfcc_log, x_axis = "time")
plt.colorbar()
plt.title("MFCC_LOG")
plt.tight_layout()
plt.show()
