#%% - SHOWS AUDIO PLOT AND SAVES WAV FILE FOR 5 SECONDS-------------------------

#%% - Imports ------------------------------------------------------------------

import pyaudio
import numpy as np
import matplotlib.pyplot as plt
import wave

#%% - Wave Signal Plot ---------------------------------------------------------

FORMAT = pyaudio.paInt16        #sampling size and format
CHANNELS = 1                    #mono
RATE = 44100                    #samples per second (sampling rate)
CHUNK = 1024*2                #bytes of data in a buffer; instant storage
RECORD_SECONDS = 5              #number of seconds active
WAVE_OUTPUT_FILENAME = "TESTE.wav"

#pyaudio class instance
p = pyaudio.PyAudio()
#stream object to get data from mic
stream = p.open(format = FORMAT, channels = CHANNELS, rate = RATE,
                frames_per_buffer = CHUNK, input = True, output = True)


#create the plot instance with figure and axis
fig, ax = plt.subplots()

#variables for plotting
x = np.arange(10000)
y = np.random.randn(10000)

#create the line object and ax plot settings
line, = ax.plot(x, y)
ax.set_xlim(0, CHUNK)
ax.set_ylim(-2**15, 2**15)
ax.set_title("Raw Audio Data")
ax.set_xlabel("samples")
ax.set_ylabel("volume")

# Show the plot, but without blocking updates
plt.pause(0.01)
plt.tight_layout()

# Open the connection and start streaming the data
stream.start_stream()
print("* recording\n")
print("\n+---------------------------------+")
print("| Press Ctrl+C to Break Recording |")
print("+---------------------------------+\n")

#Vector to save the data in recorded audio
frames = []


for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):   #active for +-5 seconds
    try:
        data = stream.read(CHUNK, exception_on_overflow = False)
        frames.append(data)
        data_int = np.frombuffer(data, dtype = np.int16)    #dtype = "h" better?
        line.set_xdata(np.arange(len(data_int)))
        line.set_ydata(data_int)
        plt.pause(0.01)
    except KeyboardInterrupt:
        break
    except:
        pass

print("\n* done recording")

# Close up shop (currently not used because KeyboardInterrupt
# is the only way to close)
stream.stop_stream()
stream.close()

p.terminate()

#%% - Saving the data in a WAV file --------------------------------------------

wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
wf.setnchannels(CHANNELS)
wf.setsampwidth(p.get_sample_size(FORMAT))
wf.setframerate(RATE)
wf.writeframes(b''.join(frames))
wf.close()
