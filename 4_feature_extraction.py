#%% - EXTRACTING FEATURES AND CREATION OF "data.csv" FILE ----------------------

import numpy as np
import csv
import os
import librosa
import pandas as pd

WD = os.getcwd()
CSV_FILE =  WD + "/data.csv"

#Generating headers for our CSV file
header = "filename spectral_centroid chroma_stft rmse"
n_mfcc = 13                 #Number of mfcc coefficients
for i in range(1, n_mfcc+1): header += f" mfcc{i}"
header += " target"
header = header.split()     #Create a list where each word is a header item

#Creating "data.csv" file and adding the header
file = open(CSV_FILE, "w", newline = "")
with file:
    writer = csv.writer(file)
    writer.writerow(header)


#Envelope function to reduce unimportant data from the audio
def envelope(y,rate,threshold):
    mask = []
    y = pd.Series(y).apply(np.abs)
    y_mean = y.rolling(window=int(rate/10),min_periods=1,center = True).mean()
    for mean in y_mean :
        if mean > threshold:
            mask.append(True)
        else:
            mask.append(False)
    return mask


#Classes
target = "clap snap whistle noise".split()


#Extracting features (mfccs) and adding to "data.csv"
for filename in os.listdir(WD + "/clean"):
    if filename[0] != ".":
        #Load sounds in dataset
        file =  WD + f"/clean/{filename}"
        signal, sr = librosa.load(file, sr = None, mono = True)

        #Applies mask
        mask = envelope(signal, sr, threshold = 0.0005)
        signal = signal[mask]
        #Feature Extraction
        spec_cent = librosa.feature.spectral_centroid(y=signal, sr=sr)
        chroma = librosa.feature.chroma_stft(y=signal, sr=sr)
        #VERSAO 0.4.2 librosa.feature.rmse
        rmse = librosa.feature.rms(y=signal)
        mfcc = librosa.feature.mfcc(y = signal, sr = sr, n_mfcc = n_mfcc)

        #"to_append" string to store the data
        to_append = f"{filename} {np.mean(spec_cent)} {np.mean(chroma)} {np.mean(rmse)}"                       #filename

        for n in mfcc: to_append += f" {np.mean(n)}"    #mfcc{i}
        if filename[0] == "c": to_append += f" {target[0]}"     #label
        if filename[0] == "s": to_append += f" {target[1]}"     #label
        if filename[0] == "w": to_append += f" {target[2]}"     #label
        if filename[0] == "n": to_append += f" {target[3]}"     #label

        #Append "to_append" list to "data.csv"
        file = open(CSV_FILE, "a", newline = "")
        with file:
            writer = csv.writer(file)
            writer.writerow(to_append.split())
