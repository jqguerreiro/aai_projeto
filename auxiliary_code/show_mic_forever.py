#%% - SHOW THE AUDIO WAVE FOREVER ----------------------------------------------

#%% - Imports ------------------------------------------------------------------

import pyaudio
import numpy as np
import matplotlib.pyplot as plt


#%% - Wave Signal in plot forever ----------------------------------------------

FORMAT = pyaudio.paInt16        #sampling size and format
CHANNELS = 1                    #mono
RATE = 44100                    #samples per second (sampling rate)
CHUNK = 1024                    #bytes of data in a buffer; instant storage

#pyaudio class instance
p = pyaudio.PyAudio()
#stream object to get data from mic
stream = p.open(format = FORMAT, channels = CHANNELS, rate = RATE,
                frames_per_buffer = CHUNK, input = True, output = True)


#create the plot instance with figure and axis
fig, ax = plt.subplots()

#variables for plotting
x = np.arange(10000)
y = np.random.randn(10000)

#create the line object and ax plot settings
line, = ax.plot(x, y)
ax.set_xlim(0, CHUNK)
ax.set_ylim(-2**15, 2**15)
ax.set_title("Raw Audio Data")

# Show the plot, but without blocking updates
plt.pause(0.01)
plt.tight_layout()

# Open the connection and start streaming the data
stream.start_stream()
print("\n+---------------------------------+")
print("| Press Ctrl+C to Break Recording |")
print("+---------------------------------+\n")

global keep_going
keep_going = True

while keep_going:       #Goes forever
    try:
        data = stream.read(CHUNK, exception_on_overflow = False)
        data_int = np.frombuffer(data, dtype = np.int16)    #dtype = "h" better?
        line.set_xdata(np.arange(len(data_int)))
        line.set_ydata(data_int)
        plt.pause(0.01)
    except KeyboardInterrupt:
        keep_going=False
    except:
        pass

# Close up shop (currently not used because KeyboardInterrupt
# is the only way to close)
stream.stop_stream()
stream.close()

p.terminate()
