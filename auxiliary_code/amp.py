#%% - Amplification ------------------------------------------------------------
import librosa

AMPLIFICATION = 20

audio_data, samp_rate = librosa.load("clap1.wav", sr = None, mono = True)

audio_data2 = audio_data*AMPLIFICATION

librosa.output.write_wav("clap_amp.wav", audio_data2, samp_rate)
