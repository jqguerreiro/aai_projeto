#%% - Removing noise from the WAV files of the Dataset -------------------------

import noisereduce as nr
import librosa
import matplotlib.pyplot as plt

#Load Audio File
audio_data, sampling_rate1 = librosa.load("audio.wav", sr = None, mono = True)
noise_data, sampling_rate2 = librosa.load("noise.wav", sr = None, mono = True)

#Amplify signal
amplification = 100
audio_data_amp = amplification*audio_data
noise_data_amp = amplification*noise_data

#Reduce noise
reduced_noise = nr.reduce_noise(audio_clip = audio_data_amp,
                                noise_clip = noise_data_amp,
                                verbose = False)

#Save the processed signal as WAV
librosa.output.write_wav("audio_amp_no_noise.wav", reduced_noise, sampling_rate1)
librosa.output.write_wav("audio_amp.wav", audio_data_amp, sampling_rate1)
librosa.output.write_wav("noise_amp.wav", noise_amp, sampling_rate1)
