#%% - DATASET SOUND PRE-PROCESSING ---------------------------------------------
#It is run only once. It generates the WAV files of the sounds of the dataset
#without noise, after being pre-processed. The output location of these WAV
#is the "output_location" although new folders were created being these:
# "claps", "snaps", "whistles". The noisy WAV files are located in the datasets
# folder i "claps_noisy", "snaps_noisy", "whistles_noisy". The noise.wav file is
# located in the datasets folder.

import os
import noisereduce as nr
import librosa


AMPLIFICATION = 10                          #Amplification constant

#Load noise WAV
noise_location = "/Users/joaoguerreiro/Desktop/projeto_final/noise.wav"
data1, sr1 = librosa.load(noise_location, sr = None, mono = True)
#Amplify noise
data1_amp = AMPLIFICATION*data1


sample_location = "/Users/joaoguerreiro/Desktop/projeto_final/sample"
clean_location = "/Users/joaoguerreiro/Desktop/projeto_final/clean/"

#Amplifies sound of datasets wav's and removes the noise
for filename in os.listdir(sample_location):
    #Load sounds WAV
    wav = sample_location + f"/{filename}"
    data2, sr2 = librosa.load(wav, sr = None, mono = True)
    #Amplify sound
    data2_amp = AMPLIFICATION*data2
    #Noise Reduction
    clean = nr.reduce_noise(audio_clip = data2_amp,
                            noise_clip = data1_amp,
                            verbose = False)
    #Save the processed signal as WAV
    librosa.output.write_wav(clean_location + f"{filename}", clean, sr2)
