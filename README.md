# AAI_Projeto

#Links Uteis:
1- https://github.com/moritzschaefer/clapper
2- https://gist.github.com/ZWMiller/53232427efc5088007cab6feee7c6e4c
3- https://gist.github.com/denisb411/cbe1dce9bc01e770fa8718e4f0dc7367
4- https://github.com/nikhiljohn10/pi-clap
5- https://stackoverflow.com/questions/40704026/voice-recording-using-pyaudio
6- https://stackoverflow.com/questions/5137497/find-current-directory-and-files-directory
7- https://www.swharden.com/wp/2016-07-19-realtime-audio-visualization-in-python/
8- https://github.com/markjay4k/Audio-Spectrum-Analyzer-in-Python
9- https://people.csail.mit.edu/hubert/pyaudio/docs/
10-https://bastibe.de/2013-05-30-speeding-up-matplotlib.html
11-https://medium.com/@mikesmales/sound-classification-using-deep-learning-8bc2aa1990b7
12-https://medium.com/@chathuranga.15/sound-event-classification-using-machine-learning-8768092beafc
13-https://pypi.org/project/noisereduce/
14-https://towardsdatascience.com/real-time-sound-event-classification-83e892cf187e
15-https://github.com/chathuranga95/SoundEventClassification
16-https://towardsdatascience.com/extract-features-of-music-75a3f9bc265d
17-https://medium.com/@sdoshi579/classification-of-music-into-different-genres-using-keras-82ab5339efe0
